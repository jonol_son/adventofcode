import unittest
from difflib import SequenceMatcher

class Claim_Object:
	pass

# # PART 1 DEMO
# part_1_claims = "#1 @ 1,3: 4x4\n#2 @ 3,1: 4x4\n#3 @ 5,5: 2x2"

# claim_objects = []
# for claim in part_1_claims.split("\n"):
# 	claim_parts = claim.split(" ")
# 	print(claim_parts)
# 	claim_object = Claim_Object()
# 	claim_object.object_id = claim_parts[0][1:]
# 	claim_object.left_edge = claim_parts[2].split(",")[0]
# 	claim_object.top_edge =claim_parts[2].split(",")[1].replace(":", "")
# 	claim_object.x = claim_parts[3].split("x")[0]
# 	claim_object.y =claim_parts[3].split("x")[1]
# 	claim_objects.append(claim_object)


# covered_coordinates = []
# total_dupclaims = 0
# for claim in claim_objects:
# 	print("\t\t" +claim.object_id)
# 	for x_spot in range(1, int(claim.x)+1):
# 		x_loc = x_spot + int(claim.left_edge)
# 		for y_spot in range(1, int(claim.y)+1):
# 			y_loc = y_spot + int(claim.top_edge)
# 			if (x_loc, y_loc) in covered_coordinates:
# 				total_dupclaims = total_dupclaims + 1
# 			covered_coordinates.append((x_loc, y_loc))
# 			print(str(x_loc)+", "+str(y_loc))

# print(covered_coordinates)
# print(total_dupclaims)

# # PART 2 DEMO
# part_1_claims = "#1 @ 1,3: 4x4\n#2 @ 3,1: 4x4\n#3 @ 5,5: 2x2"

# claim_objects = []
# for claim in part_1_claims.split("\n"):
# 	claim_parts = claim.split(" ")
# 	print(claim_parts)
# 	claim_object = Claim_Object()
# 	claim_object.object_id = claim_parts[0][1:]
# 	claim_object.left_edge = claim_parts[2].split(",")[0]
# 	claim_object.top_edge =claim_parts[2].split(",")[1].replace(":", "")
# 	claim_object.x = claim_parts[3].split("x")[0]
# 	claim_object.y =claim_parts[3].split("x")[1]
# 	claim_object.isValid = True
# 	# claim_objects.append(claim_object)
# 	claim_object.coordinates = set([])
# 	for x_spot in range(1, int(claim_object.x)+1):
# 		x_loc = x_spot + int(claim_object.left_edge)
# 		for y_spot in range(1, int(claim_object.y)+1):
# 			y_loc = y_spot + int(claim_object.top_edge)
# 			claim_object.coordinates.add((x_loc, y_loc))


# 	for existing_claim_object in claim_objects:
# 		if existing_claim_object.isValid or claim_object.isValid:
# 			if set(existing_claim_object.coordinates).intersection(claim_object.coordinates):
# 				existing_claim_object.isValid = False
# 				claim_object.isValid = False

# 	claim_objects.append(claim_object)
# 	print (claim_object.coordinates)
# for claim_object in claim_objects:
# 	print (claim_object.object_id + " " + str(claim_object.isValid))


# # part 1		
# f= open("day3_input.txt","r")
# if f.mode == 'r':
# 	contents =f.read()
# 	claim_objects = []
# 	for claim in contents.split("\n"):
# 		claim_parts = claim.split(" ")
# 		print(claim_parts)
# 		claim_object = Claim_Object()
# 		claim_object.object_id = claim_parts[0][1:]
# 		claim_object.left_edge = claim_parts[2].split(",")[0]
# 		claim_object.top_edge =claim_parts[2].split(",")[1].replace(":", "")
# 		claim_object.x = claim_parts[3].split("x")[0]
# 		claim_object.y =claim_parts[3].split("x")[1]
# 		claim_objects.append(claim_object)

# 	covered_coordinates = []
# 	seen_coordinates = []
# 	total_dupclaims = 0
# 	for claim in claim_objects:
# 		print("\t\t" +claim.object_id)
# 		for x_spot in range(1, int(claim.x)+1):
# 			x_loc = x_spot + int(claim.left_edge)
# 			for y_spot in range(1, int(claim.y)+1):
# 				y_loc = y_spot + int(claim.top_edge)
# 				if (x_loc, y_loc) in covered_coordinates and (x_loc, y_loc) not in seen_coordinates:
# 					total_dupclaims = total_dupclaims + 1
# 					seen_coordinates.append((x_loc, y_loc))
# 				covered_coordinates.append((x_loc, y_loc))
# 				# print(str(x_loc)+", "+str(y_loc))

# 	print(covered_coordinates)
# 	print(total_dupclaims)

# part 2	
f= open("day3_input.txt","r")
if f.mode == 'r':
	contents =f.read()
	claim_objects = []
	for claim in contents.split("\n"):
		claim_parts = claim.split(" ")
		print(claim_parts)
		claim_object = Claim_Object()
		claim_object.object_id = claim_parts[0][1:]
		claim_object.left_edge = claim_parts[2].split(",")[0]
		claim_object.top_edge =claim_parts[2].split(",")[1].replace(":", "")
		claim_object.x = claim_parts[3].split("x")[0]
		claim_object.y =claim_parts[3].split("x")[1]
		claim_object.isValid = True
		# claim_objects.append(claim_object)
		claim_object.coordinates = set([])
		for x_spot in range(1, int(claim_object.x)+1):
			x_loc = x_spot + int(claim_object.left_edge)
			for y_spot in range(1, int(claim_object.y)+1):
				y_loc = y_spot + int(claim_object.top_edge)
				claim_object.coordinates.add((x_loc, y_loc))


		for existing_claim_object in claim_objects:
			if existing_claim_object.isValid or claim_object.isValid:
				if set(existing_claim_object.coordinates).intersection(claim_object.coordinates):
					existing_claim_object.isValid = False
					claim_object.isValid = False

		claim_objects.append(claim_object)
		# print (claim_object.coordinates)
	for claim_object in claim_objects:
		print (claim_object.object_id + " " + str(claim_object.isValid))