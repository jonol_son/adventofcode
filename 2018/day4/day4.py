
class Entry:
	pass

def load_actions(data):
	data_lines = sorted(data.split("\n"))
	guard_action_list = {}
	guard_nap_duration = {}
	curr_guard = ""
	for data_line in data_lines:
		entry = Entry()
		# print(data_line)
		# [1518-11-01 00:00] Guard #10 begins shift
		data_blocks = data_line.split(" ")
		# print(data_blocks)
		entry.date = data_blocks[0].replace("[","")
		entry.time = data_blocks[1].replace("]","")

		if data_blocks[2] == "Guard":
			curr_guard = data_blocks[3]
			entry.action = "start"

			if data_blocks[3] not in guard_action_list:
				guard_action_list[curr_guard] = []
				guard_nap_duration[curr_guard] = 0

		else:
			entry.action = data_blocks[2] + data_blocks[3]
			if entry.action == "wakesup":
				end_time =int(entry.time.split(":")[1])
				start_time = int(guard_action_list[curr_guard][-1].time.split(":")[1])
				# print( str(end_time) +"-"+str(start_time) )
				entry.nap_duration = end_time- start_time
				guard_nap_duration[curr_guard] = guard_nap_duration[curr_guard] + entry.nap_duration
				entry.slept_times = range(start_time, end_time)
		entry.raw = data_blocks
		guard_action_list[curr_guard].append(entry)
	return guard_action_list, guard_nap_duration

# part 1 demo	
f= open("day4_demo_input.txt","r")
if f.mode == 'r':
	data =f.read()
	guard_action_list, guard_nap_duration = load_actions(data)

	nappiest_guard = max(guard_nap_duration, key=lambda key: guard_nap_duration[key])
	print(nappiest_guard +" "+ str(guard_nap_duration[nappiest_guard]) + "min")
	nap_windows = guard_action_list[nappiest_guard]
	sleep_counts = {}
	for each in nap_windows:
		try:
			for time in each.slept_times:
				if time not in sleep_counts:
					sleep_counts[time] = 1
				else:
					sleep_counts[time] = sleep_counts[time] + 1
		except AttributeError:
			pass
	
	sleepiest_time = max(sleep_counts, key=lambda key: sleep_counts[key])
	print(sleepiest_time)
	guard_id = int(nappiest_guard.replace("#",""))
	solution = guard_id*sleepiest_time
	print(str(guard_id)+ "*"+str(sleepiest_time)+"="+str(solution))

# part 1	
f= open("day4_input.txt","r")
if f.mode == 'r':
	data =f.read()
	guard_action_list, guard_nap_duration = load_actions(data)

	nappiest_guard = max(guard_nap_duration, key=lambda key: guard_nap_duration[key])
	print(nappiest_guard +" "+ str(guard_nap_duration[nappiest_guard]) + "min")
	nap_windows = guard_action_list[nappiest_guard]
	sleep_counts = {}
	for each in nap_windows:
		try:
			for time in each.slept_times:
				if time not in sleep_counts:
					sleep_counts[time] = 1
				else:
					sleep_counts[time] = sleep_counts[time] + 1
		except AttributeError:
			pass
	
	sleepiest_time = max(sleep_counts, key=lambda key: sleep_counts[key])
	print(sleepiest_time)
	guard_id = int(nappiest_guard.replace("#",""))
	solution = guard_id*sleepiest_time
	print(str(guard_id)+ "*"+str(sleepiest_time)+"="+str(solution))

# part 2 demo	
f= open("day4_demo_input.txt","r")
if f.mode == 'r':
	data =f.read()
	guard_action_list, guard_nap_duration = load_actions(data)
	overall_sleepiest_time = 0
	overall_sleepiest_time_count = 0
	overall_sleepiest_guard = 0
	for guard in guard_action_list:
		nap_windows = guard_action_list[guard]
		sleep_counts = {}
		for each in nap_windows:
			try:
				for time in each.slept_times:
					if time not in sleep_counts:
						sleep_counts[time] = 1
					else:
						sleep_counts[time] = sleep_counts[time] + 1
			except AttributeError:
				pass
		
		sleepiest_time = max(sleep_counts, key=lambda key: sleep_counts[key])
		print(str(sleepiest_time) + " times: "+ str(sleep_counts[sleepiest_time]) + " currHigh: "+str(overall_sleepiest_time))
		if sleep_counts[sleepiest_time] > overall_sleepiest_time_count:
			overall_sleepiest_time=sleepiest_time
			overall_sleepiest_time_count=sleep_counts[sleepiest_time]
			overall_sleepiest_guard = guard
			print(str(sleepiest_time) + " " + str(sleep_counts[sleepiest_time]))


		# each_guards_sleepiest_time[guard] = sleepiest_time
	print(overall_sleepiest_time)
	guard_id = int(guard.replace("#",""))
	solution = guard_id*overall_sleepiest_time
	print(str(guard_id)+ "*"+str(overall_sleepiest_time)+"="+str(solution))



# part 2	
f= open("day4_input.txt","r")
if f.mode == 'r':
	data =f.read()
	guard_action_list, guard_nap_duration = load_actions(data)
	overall_sleepiest_time = 0
	overall_sleepiest_time_count = 0
	overall_sleepiest_guard = 0
	for guard in guard_action_list:
		print(guard)
		nap_windows = guard_action_list[guard]
		sleep_counts = {}
		for each in nap_windows:
			try:
				for time in each.slept_times:
					if time not in sleep_counts:
						sleep_counts[time] = 1
					else:
						sleep_counts[time] = sleep_counts[time] + 1
			except AttributeError:
				pass
		if sleep_counts:
			# print(sleep_counts)
			sleepiest_time = max(sleep_counts, key=lambda key: sleep_counts[key])
			print(str(sleepiest_time) + " times: "+ str(sleep_counts[sleepiest_time]) + " currHigh: "+str(overall_sleepiest_time) + "/" + str(overall_sleepiest_time_count))
			if sleep_counts[sleepiest_time] > overall_sleepiest_time_count:
				overall_sleepiest_time=sleepiest_time
				overall_sleepiest_time_count=sleep_counts[sleepiest_time]
				overall_sleepiest_guard = guard
				print("\t"+guard+" "+str(sleepiest_time) + " " + str(sleep_counts[sleepiest_time]))


		# each_guards_sleepiest_time[guard] = sleepiest_time
	print(overall_sleepiest_time)
	guard_id = int(overall_sleepiest_guard.replace("#",""))
	solution = guard_id*overall_sleepiest_time
	print(str(guard_id)+ "*"+str(overall_sleepiest_time)+"="+str(solution))