from string import ascii_uppercase
from collections import OrderedDict

class Coord:
	pass

def processData(data):
	processed_data = {}
	split_data = sorted(data.split("\n"))
	for d in split_data:
		print("("+d+")", end=",")
	max_x = 0
	min_x = 100
	max_y_coord = []
	min_y_coord = []
	max_y = 0
	min_y = 100
	max_x_coord = []
	min_x_coord = []

	for index in range(0, len(split_data)):
		coord = Coord()
		coord.isInfinite = False
		coord.name = index+1
		coord.x = int(split_data[index].split(",")[0].replace(" ",""))
		coord.y = int(split_data[index].split(",")[1].replace(" ",""))
		coord.raw = split_data[index]
		print("Coord: " + str(coord.x) + "," + str(coord.y))
		if coord.y > max_y:
			max_y = coord.y
			max_y_coord = [index+1]
		elif coord.y == max_y:
			max_y = coord.y	
			max_y_coord.append(index+1)

		if coord.x > max_x:
			max_x = coord.x
			max_x_coord = [index+1]
		elif coord.x == max_x:
			max_x = coord.x
			max_x_coord.append(index+1)

		if coord.y < min_y:
			min_y = coord.y
			min_y_coord = [index+1]
		elif coord.y == min_y:
			min_y = coord.y
			min_y_coord.append(index+1)

		if coord.x < min_x:
			min_x = coord.x
			min_x_coord = [index+1]
		elif coord.x == min_x:
			min_x = coord.x
			min_x_coord.append(index+1)

		processed_data[index+1] = coord

	infinite = list(set(max_y_coord + max_x_coord + min_y_coord + min_x_coord))
	print(infinite)
	for index in infinite:
		processed_data[index].isInfinite = True
		print("\t"+str(processed_data[index].x) + "," + str(processed_data[index].y))


	min_coord = Coord()
	min_coord.x = 0
	min_coord.y = 0

	max_coord = Coord()
	max_coord.x = max_x
	max_coord.y = max_y
	window = {'min_coord':min_coord, 'max_coord':max_coord}


	return processed_data, window


def checkIfPointIsALocation(processed_data, x,y):
	for location in processed_data:
		if processed_data[location].x == x and processed_data[location].y == y:
			# print("HIT : "+ str(processed_data[location].x) + ","+str(processed_data[location].y))
			return location
	return None


def findClosest(processed_data, x,y):
	min_dist = 100000
	min_dist_list = []
	for location in processed_data:
		distance = abs(processed_data[location].x - x) + abs(processed_data[location].y-y)
		if distance < min_dist:
			min_dist = distance
			min_dist_list = [location]
		elif distance == min_dist:
			min_dist_list.append(location)

	if len(min_dist_list) == 1:
		return min_dist_list[0]
	return None

def calculateDistance(processed_data, x,y):
	distance_sum = 0
	for location in processed_data:
		distance = abs(processed_data[location].x - x) + abs(processed_data[location].y-y)
		distance_sum = distance + distance_sum

	return distance_sum

def getCellOwnership(processed_data, window, scaler):
	cell_ownership = {}
	for y in range(window['min_coord'].y - int(100 * scaler), window['max_coord'].y+int((window['max_coord'].y*scaler))):
		for x in range(window['min_coord'].x - int(100 * scaler), window['max_coord'].x+int((window['max_coord'].x*scaler))):
			isLocation = checkIfPointIsALocation(processed_data, x,y)
			if isLocation:
				cell_ownership[str(x)+","+str(y)] = isLocation
				
			else: 
				closest = findClosest(processed_data, x,y)
				if closest:
					cell_ownership[str(x)+","+str(y)] = closest

	return cell_ownership

def getCellDistanceSum(processed_data, window):
	count = 0
	for y in range(window['min_coord'].y, window['max_coord'].y):
		for x in range(window['min_coord'].x, window['max_coord'].x):

			dist = calculateDistance(processed_data, x,y)
			
			if dist <10000:
				count = count + 1

	return count

# # part 1 
# f = open("input.txt","r")
# if f.mode == 'r':
# 	data =f.read()
# 	processed_data, window = processData(data)
	
# 	cell_ownership = getCellOwnership(processed_data, window, .5)	
# 	cell_ownership2 = getCellOwnership(processed_data, window, .75)

# 	keycount2 = {}
# 	for key in cell_ownership2:
# 		if cell_ownership2[key] not in keycount2:
# 			keycount2[cell_ownership2[key]] = 0

# 		if not processed_data[cell_ownership2[key]].isInfinite:
# 			keycount2[cell_ownership2[key]] = keycount2[cell_ownership2[key]] + 1
	
# 	keycount = {}
# 	for key in cell_ownership:
# 		if cell_ownership[key] not in keycount:
# 			keycount[cell_ownership[key]] = 0

# 		if not processed_data[cell_ownership[key]].isInfinite:
# 			keycount[cell_ownership[key]] = keycount[cell_ownership[key]] + 1

# 	toPop = []
# 	for key in keycount:
# 		print(str(key) + ": "+str(keycount[key]) + " " + str(keycount2[key]))
# 		if keycount[key] != keycount2[key]:
# 			toPop.append(key)

# 	print(str(max(keycount, key=lambda key: keycount[key])) + ": "+str(processed_data[max(keycount, key=lambda key: keycount[key])].x) + "," + str(processed_data[max(keycount, key=lambda key: keycount[key])].y) + " " +str(keycount[max(keycount, key=lambda key: keycount[key])]) )
# 	print(str(max(keycount2, key=lambda key: keycount2[key])) + ": "+str(processed_data[max(keycount2, key=lambda key: keycount2[key])].x) + "," + str(processed_data[max(keycount2, key=lambda key: keycount2[key])].y) + " " +str(keycount2[max(keycount2, key=lambda key: keycount2[key])]) )

# 	for pop in toPop:
# 		keycount.pop(pop, None)

# 	print(keycount)
# 	print(str(max(keycount, key=lambda key: keycount[key])) + ": "+str(processed_data[max(keycount, key=lambda key: keycount[key])].x) + "," + str(processed_data[max(keycount, key=lambda key: keycount[key])].y) + " " +str(keycount[max(keycount, key=lambda key: keycount[key])]) )
# 	

# part 2
f = open("input.txt","r")
if f.mode == 'r':
	data =f.read()
	processed_data, window = processData(data)
	cell_ownership = getCellDistanceSum(processed_data, window)	
	print(cell_ownership)
	