def calculateDistance(coord1, coord2):
	distance = abs(coord1[0] - coord2[0]) + abs(coord1[1] - coord2[1]) + abs(coord1[2] - coord2[2]) + abs(coord1[3] - coord2[3])
	return distance

def calculateDistances(constalations, coord):
	if not constalations:
		constalations.append([coord])
		return constalations

	closest = []
	for constalation_index in range(0, len(constalations)):
		constalation = constalations[constalation_index]
		for point_index in range(0, len(constalation)):
			point = constalation[point_index]
			if calculateDistance(point, coord) <= 3:
				closest.append(constalation_index)			

	closest = list(set(closest))
	if closest:
		# appendOrMerge()
		if len(closest) == 1:
			constalations[closest[0]].append(coord)
		# merge
		else:
			temp = []
			for close in sorted(closest)[::-1]:
				temp = temp + constalations.pop(close)
			constalations.append(temp)
	else:
		constalations.append([coord])

	return constalations


f = open("input.txt","r")
data =f.read()

constalations = []

for row in data.split("\n"):
	coords = row.split(",")
	coord = (int(coords[0]),int(coords[1]),int(coords[2]),int(coords[3]))
	print(coord)
	constalations = calculateDistances(constalations, coord)
	print (constalations)

print(len(constalations))