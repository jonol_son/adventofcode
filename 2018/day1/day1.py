import unittest



def parse_commands(delimiter, moves):
	moves = moves.split(delimiter)
	print (moves)
	parsed_moves = []
	for move in moves:
		move = move.strip()
		operator = move[0]
		digit = int(move[1:])
		move_tuple = (operator, digit)
		parsed_moves.append(move_tuple)
	
	return parsed_moves

def apply_move(move, result):
	if move[0] == "-":
		result = result - move[1]
	elif move[0] == "+":
		result = result + move[1]
	return result

def apply_all_moves(inital, parsed_moves):
	result = inital
	for move in parsed_moves:
		result = apply_move(move, result)
	return result

def find_duplicate(inital, parsed_moves):
	result = inital
	seen = [inital]
	while(1):
		for move in parsed_moves:
			result = apply_move(move, result)
			print(result)
			if result in seen:
				return result
			seen.append(result)

part_1_moves = {
	"+1, +1, +1": 3,
	"+1, +1, -2": 0,
	"-1, -2, -3": -6,
	}

part_2_moves = {
	"+1, -1": 0,
	"+3, +3, +4, -2, -4": 10,
	"-6, +3, +8, +5, -6": 5,
	"+7, +7, -2, -7, -4": 14
	}


class TestMoves(unittest.TestCase):
	def test_part_1_input(self):
		for move in part_1_moves:
			parsed_moves = parse_commands(",", move)
			initial = 0
			result = apply_all_moves(initial, parsed_moves)
			self.assertEqual(result, part_1_moves[move])
	def test_part_2_input(self):
		for move in part_2_moves:
			parsed_moves = parse_commands(",", move)
			initial = 0
			result = find_duplicate(initial, parsed_moves)
			self.assertEqual(result, part_2_moves[move])

# # part 1		
# f= open("day1_input.txt","r")
# if f.mode == 'r':
# 	contents =f.read()
# 	print(contents)
# 	parsed_moves = parse_commands("\n", contents)
# 	initial = 0
# 	result = apply_all_moves(initial, parsed_moves)
# 	print (str(result) )

# # part 2	
# f= open("day1_input.txt","r")
# if f.mode == 'r':
# 	contents =f.read()
# 	parsed_moves = parse_commands("\n", contents)
# 	initial = 0
# 	# result = apply_all_moves(initial, parsed_moves)
# 	result = find_duplicate(initial, parsed_moves)
# 	print (str(result) )
# 	# 66105

