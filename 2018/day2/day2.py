import unittest
from difflib import SequenceMatcher

def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

def parse_code(code, parsed_codes):
	code = code.strip().lower()
	two_count = False
	three_count = False

	while(code):
		count = code.count(code[0])
		if count == 2 and two_count is False:
			parsed_codes[2] = parsed_codes[2] + 1
			two_count = True
		if count == 3 and three_count is False:
			parsed_codes[3] = parsed_codes[3] + 1
			three_count = True

		code = code.replace(code[0], '')
	print (code + ": " + str(parsed_codes))
	return parsed_codes

def get_similar(code, similarity_object):
	code = code.strip().lower()
	for each in similarity_object['seen']:
		diff = similar(each, code)
		if diff >= similarity_object["similarity"]:
			similarity_object["similarity"]= diff
			similarity_object["id1"]= code
			similarity_object["id2"]= each
	similarity_object["seen"].append(code)
	print(similarity_object)
	return similarity_object

part_1_codes = {
	"abcdef": (0,0),
	"bababc": (1,1),
	"abbcde": (1,0),
	"abcccd": (0,1),
	"aabcdd": (1,0),
	"abcdee": (1,0),
	"ababab": (0,1)
	}

part_2_codes = [
	"abcde",
	"fghij",
	"klmno",
	"pqrst",
	"fguij",
	"axcye",
	"wvxyz"
	]

similarity_object = {
	"similarity" : 0,
	"id1" : "",
	"id2" : "",
	"seen" : []
}

class TestMoves(unittest.TestCase):
	def test_part_1_input(self):
		parsed_codes = {2: 0, 3:0}
		for code in part_1_codes:
			parsed_code = parse_code(code, parsed_codes)
		
		three_count = parsed_code[3]
		two_count = parsed_code[2]
		result = three_count * two_count
		print(str(three_count) +"*"+ str(two_count) + " = "+ str(result))
		self.assertEqual(two_count * three_count, 12)

	def test_part_2_input(self):
		similarity_object = {
			"similarity" : 0,
			"id1" : "",
			"id2" : "",
			"seen" : []
		}

		for code in part_2_codes:
			similarity_object = get_similar(code, similarity_object)

		result = ""
		for index in range(0, len(similarity_object['id1'])):
			if similarity_object['id1'][index] == similarity_object['id2'][index]:
				result = result + (similarity_object['id1'][index])
		
		self.assertEqual(result, "fgij")


# part 1		
f= open("day2_input.txt","r")
if f.mode == 'r':
	contents =f.read()
	print(contents)
	parsed_codes = {2: 0, 3:0}
	for code in contents.split("\n"):
		parsed_code = parse_code(code, parsed_codes)
	three_count = parsed_code[3]
	two_count = parsed_code[2]
	result = three_count * two_count
	print(str(three_count) +"*"+ str(two_count) + " = "+ str(result))

# part 2	
f= open("day2_input.txt","r")
if f.mode == 'r':
	contents =f.read()
	for code in contents.split("\n"):
		similarity_object = get_similar(code, similarity_object)
	result = ""
	for index in range(0, len(similarity_object['id1'])):
		if similarity_object['id1'][index] == similarity_object['id2'][index]:
			result = result + (similarity_object['id1'][index])
	print(str(similarity_object["similarity"]) + ": " + similarity_object["id1"] + " " + similarity_object["id2"])
	print(result)