from string import ascii_uppercase
from collections import OrderedDict






def getAndApplySpoiler(data):
	
	spoiler = data.swapcase()
	for index in range(0, len(data)):

		if(index != 0):
			# compare prev
			if spoiler[index-1] == data[index]:
				# print("compare prev: " +spoiler[index-1] + "->" + data[index-1]+data[index]+data[index+1])
				data = data[:index-1] + data[index+1:]
				return data
		if(index != len(data)-1):
			# compare next
			if spoiler[index+1] == data[index]:
				# print("compare next: " +spoiler[index+1] + "->" + data[index-1]+data[index]+data[index+1])
				data = data[:index] + data[index+2:]
				return data

	return data

# # part 1 demo	
# f = open("demo_input.txt","r")
# if f.mode == 'r':
# 	data =f.read()
# 	result = ""
# 	diff = True
# 	while(diff):
# 		print(data)
# 		result = getAndApplySpoiler(data)
# 		if result == data:
# 			diff = False

# 		data = result
# 	print(result)

# # part 1	
# f = open("input.txt","r")
# if f.mode == 'r':
# 	data =f.read()
# 	result = ""
# 	diff = True
# 	while(diff):
# 		print(data)
# 		result = getAndApplySpoiler(data)
# 		if result == data:
# 			diff = False

# 		data = result
# 	print(result)
# 	print(len(result))

# # part 2 demo	
# f = open("demo_input.txt","r")
# if f.mode == 'r':
# 	data =f.read()
# 	alpha_dict = OrderedDict((ch, 0) for idx, ch in enumerate(ascii_uppercase, 0))
# 	for letter in alpha_dict:
# 		result = ""
# 		diff = True
# 		# STRIP LETTER
# 		data_without_letter = data.replace(letter,"").replace(letter.lower(),"")
# 		while(diff):
# 			# print(data_without_letter)
# 			result = getAndApplySpoiler(data_without_letter)
# 			if result == data_without_letter:
# 				diff = False

# 			data_without_letter = result
	
# 		print(letter + ": "+str(len(result)))
# 		alpha_dict[letter]=len(result)

# 	print(alpha_dict)
# 	print( min(alpha_dict, key=lambda key: alpha_dict[key]))

# part 2 
f = open("input.txt","r")
if f.mode == 'r':
	data =f.read()
	alpha_dict = OrderedDict((ch, 0) for idx, ch in enumerate(ascii_uppercase, 0))
	for letter in alpha_dict:
		result = ""
		diff = True
		# STRIP LETTER
		data_without_letter = data.replace(letter,"").replace(letter.lower(),"")
		while(diff):
			# print(data_without_letter)
			result = getAndApplySpoiler(data_without_letter)
			if result == data_without_letter:
				diff = False

			data_without_letter = result
	
		print(letter + ": "+str(len(result)))
		alpha_dict[letter]=len(result)

	print(alpha_dict)
	print( min(alpha_dict, key=lambda key: alpha_dict[key]))